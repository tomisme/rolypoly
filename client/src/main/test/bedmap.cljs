(ns test.bedmap
  (:require
    ["react-native" :as rn]
    [reagent.core :as r]
    [promesa.core :as p]))
(def _)

(def styles
  ^js (-> {
           :title
           {:fontSize 100}

           :centered
           {:flex 1
            :alignItems "center"
            :justifyContent "center"}}

          (clj->js)
          (rn/StyleSheet.create)))

(defonce state (r/atom {:loading-data? false
                        :data nil
                        :toggle true
                        :bed-num 1}))

(defn process-fetch [data]
  (swap! state assoc :data (js->clj data :keywordize-keys true)))

; 10.0.2.2 is laptop localhost from inside Android emulator
(def bed-uri
  ; "http://10.0.2.2:3002/api/beds"
  "https://rolypoly.appspot.com/api/beds")

(defn on-fetch-press []
  (p/let [_ (swap! state assoc :loading-data? true)
          _ (-> (js/fetch bed-uri)
                (p/then #(.json %))
                (p/then process-fetch)
                (p/catch js/console.error))
          _ (swap! state assoc :loading-data? false)]))

(defn bed-icon [emoji]
  [:> rn/Text {:style {:fontSize 24}}
   emoji])

(defn trunc [s n]
  (apply str (take n s)))

(defn bed-view [bed-data]
  (let [{:keys [mostRecentPlanting]} bed-data]
    (condp #(.includes (or %2 "") %1) mostRecentPlanting
      "CORN" (bed-icon "🌽")
      [:> rn/Text {:style {:fontSize 12}}
       (trunc mostRecentPlanting 25)])))

(defn filter-bay [num coll]
  (filter #(= (:bay %) num) coll))

(defn bedmap-grid-view [{:keys [data active-bed bed-px]}]
  (into [:> rn/View {:style {:postion "absolute"}}]
        (for [{:keys [run bed] :as bed-data}
              (filter-bay active-bed data)]
          [:> rn/View {:style
                       (clj->js
                        {:position "absolute"
                         :width 35
                         :left (+ 50 (* (- 6 run) (* bed-px 1.8)))
                         :top (+ 100 (* (- 6 bed) (* bed-px 3)))})}
           (bed-view bed-data)])))

(defn toggle [prev]
  (if prev
    false
    true))

(defn bedmap-list-view [{:keys [data]}]
  [:> rn/SafeAreaView
   [:> rn/View {:style {:flex-direction "row"}}
    [:> rn/Button {:on-press #(swap! state assoc :data nil)
                   :title "back"}]
    [:> rn/Button {:on-press #(swap! state update :toggle toggle)
                   :title (str "show map")}]]
   (into [:> rn/ScrollView {:style {:margin 10}}]
         (for [{:keys [bay run bed mostRecentPlanting]} data]
           [:> rn/Text
            (str bay "/" run "/" bed ": " mostRecentPlanting)]))])

(defn bedmap-view []
  (if-let [data (:data @state)]
    (let [s @state
          active-bed (:bed-num s)
          tog (:toggle s)]
      (if tog
        (bedmap-list-view {:data data})
        [:> rn/View
         (bedmap-grid-view {:data data
                            :active-bed active-bed
                            :bed-px 30})
         [:> rn/View {:style {:flex-direction "row"}}
          [:> rn/Button {:on-press #(swap! state assoc :data nil)
                         :title "back"}]
          [:> rn/Button {:on-press #(swap! state update :toggle toggle)
                         :title (str "show list")}]
          [:> rn/Button {:on-press #(swap! state update :bed-num dec)
                         :title "prev bed"}]
          [:> rn/Button {:on-press #(swap! state update :bed-num inc)
                         :title "next bed"}]]
         [:> rn/Text {:style {:fontSize 24
                              :marginTop 10
                              :textAlign "center"}}
          (str "Bed " active-bed)]]))
    (let [loading? (:loading-data? @state)]
      [:> rn/View {:style (.-centered styles)}
       [:> rn/Text {:style (.-title styles)}
        (if loading? "🔥" "🐢")]
       (when (not loading?)
         [:> rn/Button {:on-press on-fetch-press
                        :title "fetch bed data"}])])))
