import gs from "google-spreadsheet";
import { buildMainFieldGrid } from "./mainField.js";

export async function buildGooper(cg) {
  const doc = new gs.GoogleSpreadsheet(cg("BED_MAP_GOOGLE_SHEETS_ID"));

  await doc.useServiceAccountAuth({
    client_email: cg("GOOP_SERVICE_ACCOUNT_EMAIL"),
    private_key: cg("GOOP_SERVICE_PRIVATE_KEY")
  });

  await doc.loadInfo();
  console.log(doc.title);

  const beds = await buildMainFieldGrid(doc);

  return { beds };
}
