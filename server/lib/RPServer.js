import _ from "lodash";
import Hapi from "@hapi/hapi";

export async function buildRPServer(cg, db) {

  const server = Hapi.server({
    port: cg("PORT"),
    host: cg("HTTP_SERVER_HOST"),
    routes: {
      cors: cg("NODE_ENV") === "development"
    }
  });

  server.route({
    method: "GET",
    path: "/api/beds",
    handler: async (request, h) => {
      return db.beds;
    }
  });

  return server;
}
