function findMostRecentPlanting(prevCell, sheet, row, column) {
  const cell = sheet.getCell(row, column);
  if (cell.value) {
    return findMostRecentPlanting(cell, sheet, row, column + 1);
  } else if (prevCell) {
    return prevCell;
  } else {
    return { value: null };
  }
}

function buildBedHistory(sheet, row, column) {

}

async function getBedDetails(sheet, runNum, bedNum) {
  const topHeaderHeight = 2;
  const rowLabelWidth = 2;

  const runIndex = runNum - 1;
  const bedIndex = bedNum - 1;

  const rowIndex = topHeaderHeight + runIndex * 7 + bedIndex;

  const mostRecentCell = findMostRecentPlanting(
    null,
    sheet,
    rowIndex,
    rowLabelWidth
  );
  const mostRecentPlanting = mostRecentCell.value;

  return {
    mostRecentCell,
    mostRecentPlanting
  };
}

export async function buildMainFieldGrid(doc) {
  let cellRequests = [];
  for (const bay of [1, 2, 3, 4, 5, 6, 7, 8, 9]) {
    const sheet = doc.sheetsByIndex[bay - 1];
    cellRequests.push(new Promise(async resolve => {
      console.log('getting bay', bay);
      await sheet.loadCells("C3:AB44");
      console.log('got bay', bay);
      resolve();
    }));
  }
  await Promise.all(cellRequests);
  console.log("loaded");

  let grid = [];
  for (const bay of [1, 2, 3, 4, 5, 6, 7, 8, 9]) {
    const sheet = doc.sheetsByIndex[bay - 1];

    for (const run of [1, 2, 3, 4, 5, 6]) {
      for (const bed of [1, 2, 3, 4, 5, 6]) {
        const { mostRecentPlanting} = await getBedDetails(sheet, run, bed);
        console.log(`${bay}:${run}:${bed}`, mostRecentPlanting);
        grid.push({bay, run, bed, mostRecentPlanting});
      }
    }
  }
  return grid;
}
