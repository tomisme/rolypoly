import config from "nconf";
import { buildGooper } from "./lib/goop.js";
import { buildRPServer } from "./lib/RPServer.js";

(async () => {
  config.argv().env();
  const cg = key => config.get(key);
  const ENV = cg("NODE_ENV");

  if (!ENV) {
    throw new Error("NODE_ENV has not been set");
  } else if (ENV === "production") {
    config.file("prod", "prod.env.json");
  } else {
    config
      .file("secrets", "secrets/secret.dev.env.json")
      .file("dev", "dev.env.json");
  }

  let db = {};

  console.log('Building RolyPoly gooper');
  const { beds } = await buildGooper(cg);

  db.beds = beds;

  console.log('Building RolyPoly server');
  const apiServer = await buildRPServer(cg, db);
  await apiServer.start();
  console.log("RolyPoly API Server running on %s", apiServer.info.uri);
})();
